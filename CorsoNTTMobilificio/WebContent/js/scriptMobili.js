function rigaTabella(objMobile) {
	
	let riga =  '<tr data-identificatore="' + objMobile.MobileID + '">';
		riga += '    <td>' + objMobile.nomeMobile + '</td>';
		riga += '    <td>' + objMobile.descrizioneMobile.substr(0, 60) + '...</td>';
		riga += '    <td>' + objMobile.codiceMobile + '</td>';
		riga += '    <td>' + objMobile.prezzo + '</td>';
		riga += '    <td><button type="button" class="btn btn-danger btn-block">E</button></td>';
		riga += '    <td><button type="button" class="btn btn-warning btn-block">M</button></td>';
		riga += '</tr>';
	
	return riga;
	
}

function stampaTabella(arrayMobili) {
	
	let contenuto = "";
	
	for(let i=0; i<arrayMobili.length; i++) {
		contenuto += rigaTabella(arrayMobili[i]);
	}
	
	$("#contenuto-tabella-mobili").html(contenuto);
	
}

$(document).ready(
	function(){
		$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTMobilificio/visualizzamobili",
				method: "POST",
				success: function(successo){
					let risJson = JSON.parse(successo);
					stampaTabella(risJson);
				},
				error: function(errore){
					console.log(errore);
				}
			}
		);
	}
)