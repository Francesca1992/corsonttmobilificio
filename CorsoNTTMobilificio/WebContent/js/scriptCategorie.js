function rigaTabella(objCategoria) {
	
	let riga =  '<tr data-identificatore="' + objCategoria.CategoriaID + '">';
		riga += '    <td>' + objCategoria.nomeCategoria + '</td>';
		riga += '    <td>' + objCategoria.descrizioneCategoria.substr(0, 60) + '...</td>';
		riga += '    <td>' + objCategoria.codiceCategoria + '</td>';
		riga += '</tr>';
	
	return riga;
	
}

function stampaTabella(arrayCategorie) {
	
	let contenuto = "";
	
	for(let i=0; i<arrayCategorie.length; i++) {
		contenuto += rigaTabella(arrayCategorie[i]);
	}
	
	$("#contenuto-tabella").html(contenuto);
	
}

$(document).ready(
	function(){
		$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTMobilificio/visualizzacategorie",
				method: "POST",
				success: function(successo){
					let risJson = JSON.parse(successo);
					stampaTabella(risJson);
				},
				error: function(errore){
					console.log(errore);
				}
			}
		);
	}
)