package com.lezione33.mobilificio.model;

import java.util.ArrayList;

public class Mobile {
	
	private int mobileID;
	private String nomeMobile;
	private String descrizioneMobile;
	private String codiceMobile;
	private float prezzo;
	private ArrayList<Categoria> categorie;
	
	public Mobile() {
		
	}

	public int getMobileID() {
		return mobileID;
	}

	public void setMobileID(int mobileID) {
		this.mobileID = mobileID;
	}

	public String getNomeMobile() {
		return nomeMobile;
	}

	public void setNomeMobile(String nomeMobile) {
		this.nomeMobile = nomeMobile;
	}

	public String getDescrizioneMobile() {
		return descrizioneMobile;
	}

	public void setDescrizioneMobile(String descrizioneMobile) {
		this.descrizioneMobile = descrizioneMobile;
	}

	public String getCodiceMobile() {
		return codiceMobile;
	}

	public void setCodiceMobile(String codiceMobile) {
		this.codiceMobile = codiceMobile;
	}

	public float getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}
	
	public ArrayList<Categoria> getCategorie() {
		return categorie;
	}

	public void setCategorie(ArrayList<Categoria> categorie) {
		this.categorie = categorie;
	}

	public String stampaMobile() {
		return "Mobile [mobileID=" + mobileID + ", nomeMobile=" + nomeMobile + ", descrizioneMobile=" + descrizioneMobile + ", codiceMobile=" + codiceMobile + ", prezzo=" + prezzo + ", categorie=" + categorie + "]";
	}
	
}