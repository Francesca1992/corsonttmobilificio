package com.lezione33.mobilificio.model;

public class Categoria {
	
	private int categoriaID;
	private String nomeCategoria;
	private String descrizioneCategoria;
	private String codiceCategoria;
	
	public Categoria(){
		
	}
	
	public int getCategoriaID() {
		return categoriaID;
	}
	public void setCategoriaID(int categoriaID) {
		this.categoriaID = categoriaID;
	}
	public String getNomeCategoria() {
		return nomeCategoria;
	}
	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}
	public String getDescrizioneCategoria() {
		return descrizioneCategoria;
	}
	public void setDescrizioneCategoria(String descrizioneCategoria) {
		this.descrizioneCategoria = descrizioneCategoria;
	}
	public String getCodiceCategoria() {
		return codiceCategoria;
	}
	public void setCodiceCategoria(String codiceCategoria) {
		this.codiceCategoria = codiceCategoria;
	}

	public String stampaCategoria() {
		return "Categoria [categoriaID=" + categoriaID + ", nomeCategoria=" + nomeCategoria + ", descrizioneCategoria="
				+ descrizioneCategoria + ", codiceCategoria=" + codiceCategoria + "]";
	}
}