package com.lezione33.mobilificio.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lezione33.mobilificio.connessioni.ConnettoreDB;
import com.lezione33.mobilificio.model.Categoria;
import com.mysql.jdbc.PreparedStatement;

public class CategoriaDAO implements Dao<Categoria> {

	@Override
	public Categoria getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Categoria getByCod(String cod) throws SQLException {
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT CategoriaID, nomeCategoria, descrizioneCategoria, codiceCategoria, FROM Categoria WHERE codiceCategoria = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, cod);
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();
       	
       	Categoria catTemp = new Categoria();
       	catTemp.setCategoriaID(risultato.getInt(1));
       	catTemp.setNomeCategoria(risultato.getString(2));
       	catTemp.setDescrizioneCategoria(risultato.getString(3));
       	catTemp.setCodiceCategoria(risultato.getString(4));
       	
       	return catTemp;
	}

	@Override
	public ArrayList<Categoria> getAll() throws SQLException {
		
		ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT categoriaID, nomeCategoria, descrizioneCategoria, codiceCategoria FROM Categoria";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ResultSet risultato = ps.executeQuery();
       	
       	while(risultato.next()) {
       		Categoria catTemp = new Categoria();
           	catTemp.setCategoriaID(risultato.getInt(1));
           	catTemp.setNomeCategoria(risultato.getString(2));
           	catTemp.setDescrizioneCategoria(risultato.getString(3));
           	catTemp.setCodiceCategoria(risultato.getString(4));
           	
           	elencoCategorie.add(catTemp);
       	}
       	
       	return elencoCategorie;
	}

	@Override
	public void insert(Categoria t) throws SQLException {
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "INSERT INTO Categoria (nomeCategoria, descrizioneCategoria, codiceCategoria) VALUES (?, ?, ?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getNomeCategoria());
		ps.setString(2, t.getDescrizioneCategoria());
		ps.setString(3, t.getCodiceCategoria());
		ps.executeUpdate();
		
	}

	@Override
	public boolean delete(Categoria t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Categoria t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}