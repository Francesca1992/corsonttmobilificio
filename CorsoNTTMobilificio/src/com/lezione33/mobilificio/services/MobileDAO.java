package com.lezione33.mobilificio.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lezione33.mobilificio.connessioni.ConnettoreDB;
import com.lezione33.mobilificio.model.Mobile;
import com.mysql.jdbc.PreparedStatement;

public class MobileDAO implements Dao<Mobile> {

	@Override
	public Mobile getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Mobile getByCod(String cod) throws SQLException {
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT mobileID, nomeMobile, descrizioneMobile, codiceMobile, prezzo FROM Mobile WHERE codiceMobile = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, cod);
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();
       	
       	Mobile mobTemp = new Mobile();
       	mobTemp.setMobileID(risultato.getInt(1));
       	mobTemp.setNomeMobile(risultato.getString(2));
       	mobTemp.setDescrizioneMobile(risultato.getString(3));
       	mobTemp.setCodiceMobile(risultato.getString(4));
       	mobTemp.setPrezzo(risultato.getFloat(5));
       	
       	return mobTemp;
	}

	@Override
	public ArrayList<Mobile> getAll() throws SQLException {
		
		ArrayList<Mobile> elencoMobili = new ArrayList<Mobile>();
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT mobileID, nomeMobile, descrizioneMobile, codiceMobile, prezzo FROM Mobile";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ResultSet risultato = ps.executeQuery();
       	
       	while(risultato.next()) {
       		Mobile mobTemp = new Mobile();
       		mobTemp.setMobileID(risultato.getInt(1));
       		mobTemp.setNomeMobile(risultato.getString(2));
       		mobTemp.setDescrizioneMobile(risultato.getString(3));
       		mobTemp.setCodiceMobile(risultato.getString(4));
       		mobTemp.setPrezzo(risultato.getFloat(5));
           	
           	elencoMobili.add(mobTemp);
       	}
       	
       	return elencoMobili;
	}

	@Override
	public void insert(Mobile t) throws SQLException {
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "INSERT INTO Mobile (nomeMobile, descrizioneMobile, codiceMobile, prezzo) VALUES (?, ?, ?, ?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getNomeMobile());
		ps.setString(2, t.getDescrizioneMobile());
		ps.setString(3, t.getCodiceMobile());
		ps.setFloat(4, t.getPrezzo());
		ps.executeUpdate();
		
	}

	@Override
	public boolean delete(Mobile t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Mobile t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}