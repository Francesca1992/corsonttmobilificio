package com.lezione33.mobilificio.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione33.mobilificio.model.Categoria;
import com.lezione33.mobilificio.services.CategoriaDAO;

@WebServlet("/visualizzacategorie")
public class VisualizzaCategorie extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		
		CategoriaDAO catDao = new CategoriaDAO();
		
		try {
			
			ArrayList<Categoria> elencoCategorie = catDao.getAll();
			
			Gson jsonizzatore = new Gson();
			String risultatoJson = jsonizzatore.toJson(elencoCategorie);
			
			out.print(risultatoJson);
			
		} catch (SQLException e) {
			
			System.out.println(e.getMessage());
		
		}
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
}