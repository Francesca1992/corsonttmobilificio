package com.lezione33.mobilificio.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione33.mobilificio.model.Mobile;
import com.lezione33.mobilificio.services.MobileDAO;

@WebServlet("/visualizzamobili")
public class VisualizzaMobili extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		
		MobileDAO mobDao = new MobileDAO();
		
		try {
			
			ArrayList<Mobile> elencoMobili = mobDao.getAll();
			
			Gson jsonizzatore = new Gson();
			String risultatoJson = jsonizzatore.toJson(elencoMobili);
			
			out.print(risultatoJson);
			
		} catch (SQLException e) {
			
			System.out.println(e.getMessage());
		
		}
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
}