DROP DATABASE IF EXISTS lez_33_mobilificio;
CREATE DATABASE lez_33_mobilificio;
USE lez_33_mobilificio;

CREATE TABLE Categoria(
	categoriaID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nomeCategoria VARCHAR(250) NOT NULL UNIQUE,
    descrizioneCategoria TEXT,
    codiceCategoria VARCHAR(20) NOT NULL UNIQUE
);

INSERT INTO Categoria (nomeCategoria, descrizioneCategoria, codiceCategoria) VALUES
("cucina", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto rerum distinctio sequi tempora autem nam eligendi! Error velit molestias suscipit, pariatur rem blanditiis a laborum saepe perferendis dolorum totam eligendi.", "CUC"),
("bagno", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto rerum distinctio sequi tempora autem nam eligendi! Error velit molestias suscipit, pariatur rem blanditiis a laborum saepe perferendis dolorum totam eligendi.", "BAG"),
("camera da letto", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto rerum distinctio sequi tempora autem nam eligendi! Error velit molestias suscipit, pariatur rem blanditiis a laborum saepe perferendis dolorum totam eligendi.", "CAM"),
("sala da pranzo", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto rerum distinctio sequi tempora autem nam eligendi! Error velit molestias suscipit, pariatur rem blanditiis a laborum saepe perferendis dolorum totam eligendi.", "SAP"),
("salotto", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto rerum distinctio sequi tempora autem nam eligendi! Error velit molestias suscipit, pariatur rem blanditiis a laborum saepe perferendis dolorum totam eligendi.", "SAL");

SELECT * FROM Categoria;

CREATE TABLE Mobile(
	mobileID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nomeMobile VARCHAR(250) NOT NULL,
    descrizioneMobile TEXT,
    codiceMobile VARCHAR(250) NOT NULL,
    prezzo FLOAT NOT NULL
);

INSERT INTO Mobile (nomeMobile, descrizioneMobile, codiceMobile, prezzo) VALUES
("sedia", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto rerum distinctio sequi tempora autem nam eligendi! Error velit molestias suscipit, pariatur rem blanditiis a laborum saepe perferendis dolorum totam eligendi.", "SED", 30.0),
("letto", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto rerum distinctio sequi tempora autem nam eligendi! Error velit molestias suscipit, pariatur rem blanditiis a laborum saepe perferendis dolorum totam eligendi.", "LET", 499.9),
("armadio", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto rerum distinctio sequi tempora autem nam eligendi! Error velit molestias suscipit, pariatur rem blanditiis a laborum saepe perferendis dolorum totam eligendi.", "ARM", 899.99),
("libreria", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto rerum distinctio sequi tempora autem nam eligendi! Error velit molestias suscipit, pariatur rem blanditiis a laborum saepe perferendis dolorum totam eligendi.", "LIB", 600),
("divano", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto rerum distinctio sequi tempora autem nam eligendi! Error velit molestias suscipit, pariatur rem blanditiis a laborum saepe perferendis dolorum totam eligendi.", "DIV", 400),
("tavolo", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto rerum distinctio sequi tempora autem nam eligendi! Error velit molestias suscipit, pariatur rem blanditiis a laborum saepe perferendis dolorum totam eligendi.", "TAV", 510);

SELECT * FROM Mobile;

CREATE TABLE Mobile_Categoria(
    idMobile INTEGER NOT NULL,
    idCategoria INTEGER NOT NULL,
    FOREIGN KEY (idCategoria) REFERENCES Categoria(categoriaID),
    FOREIGN KEY (idMobile) REFERENCES Mobile(mobileID),
    PRIMARY KEY(idMobile, idCategoria)
);

